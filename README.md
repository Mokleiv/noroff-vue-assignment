# Noroff Assignment 5: Frontend Development with Vue.

 ## Description:
 This project uses Vue3 to build at single-page-webapplication for a trivia game website.
 The webpage fetches questions from 'https://opentdb.com/api_config.php' and gives the user
 the option to choose difficulty, category and number of questions.

## To get started with this project, first download or close this repo, then run:
 - npm install
 - npm run dev

## Authors:
 - Sondre Mokleiv Nygård
 - Ina F. Pedersen
